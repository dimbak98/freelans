<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderModel extends Model
{
    protected $guarded=[];

    public function order(){
        return $this->hasMany('App\User');
    }
}
