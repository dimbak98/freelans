@extends('admin.layouts.app_admin')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Категорий 0</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Заказов 0</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Поситителей 0</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Сегодня 0</span></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <a class="bth bth-block bth-default" href="#">Создать категорию</a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Категория первая</h4>
                    <p class="list-group-item-text">
                        Кол-во категорий
                    </p>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <a class="bth bth-block bth-default" href="#">Создать заказ</a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Заказ первый</h4>
                    <p class="list-group-item-text">
                        Кол-во материалов
                    </p>
                </a>
            </div>
        </div>
    </div>
@endsection